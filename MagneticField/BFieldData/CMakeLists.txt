################################################################################
# Package: BFieldData
################################################################################

# Declare the package name:
atlas_subdir( BFieldData )

# Install files from the package:
atlas_install_runtime( share/magempty share/magfieldfull )

